var loader = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>'),
    EV = ('ontouchstart' in document.documentElement) ? 'touchend touchcancel click' : 'click',
    currentDate = new Date(),
    cYear = currentDate.getFullYear(),
    icon_boxes = ".uslovie-prodazhi-box, " +
        ".san-uzel-box, " +
        ".planirovka-box, " +
        ".tip-doma-box, " +
        ".remont-box, " +
        ".agenstva-box, " +
        ".stations-box, " +
        ".microraions-box";
sessionStorage.sortColToPrint = 'data_postupleniya';
sessionStorage.ascDesc = 'DESC';
var contact_html;

(function ($) {
    $.expr[':'].contains = function (a, i, m) {
        return $(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
    };

    function uncheckAllCustom(o) {
        o.next().find("input[type='checkbox']").prop("checked", false);
        o.hide();
    }

    function setToggleIconBox(checked_elements, filter_box) {
        var o = checked_elements.parents(filter_box);
        (o.find("input:checkbox:checked").length > 0) ? o.prev().show() : o.prev().hide();
    }

    var setPlaceholders = function (name, default_text) {
        text = '';
        $("." + name + "-filter input:checkbox:checked").siblings(".option").each(function () {
            text += ($("." + name + "-filter input:checkbox:checked").length > 1) ? $(this).text() + ', ' : $(this).text();
        });

        if (text == '') {
            text = default_text;
        }
        $("." + name + "-select option").html('<span>' + text + '</span>');
    };
    var setPlaceholdersCities = function (name, default_text) {
        text = '';
        $("." + name + "-filter input:checkbox:checked").siblings(".option").each(function () {
            text += ($("." + name + "-filter input:checkbox:checked").length > 1) ? $(this).text() + ', ' : $(this).text();
        });

        if (text == '') {
            text = default_text;
        }
        $("." + name + "-select").html('<span>' + text + '</span>');
    };
    var user_logout = function () {
        alert('В аккаунт вошли с другого компьютера.');
        document.location.href = 'user';
    };
    var user_maintenance = function () {
        alert('В данный момент на сервере обновляются данные. Большинство функций системы пока не доступны. Попробуйте повторить попытку через 2-3 минуты.');
    };

    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $(".overlay-main").hide();
            $(".banner-big").hide();
            $(".popup-lists-add").hide();
            $(".popup-lists-edit").hide();
            $(".popup-kart-edit").hide();
            $(".popup-contact-edit").hide();
            $(".contacts-menu-box").hide();
            $(".contacts-arrow").hide();
            $(".contact-wrapper").css("background", "");
        }   // esc

        if ($(e.target).attr("class") == "new-list-val") {
            if (e.keyCode == 13) {
                $(".new-list-ok").trigger('click');
            }   // enter
        }

        if ($(e.target).attr("class") == "edit-list-val") {
            if (e.keyCode == 13) {
                $(".edit-list-ok").trigger('click');
            }   // enter
        }

        if ($(e.target).attr("class") == "popup-lists-find") {
            if (e.keyCode != 13 && e.keyCode != 27) {
                var find_value = $(".popup-lists-find").val(),
                    find_container = $('.popup-lists .list-wrapper:contains("' + find_value + '")');

                if (find_value) {
                    if (find_container.html()) {
                        find_block = '';
                        find_container.each(function () {
                            find_block += '<div class="list-wrapper">' + $(this).html() + '</div>';
                        });
                        $(".popup-lists-find-container").html(find_block);
                        find_container.hide();

                        $(".popup-lists-find-container .list-id").each(function () {
                            $(this).html($(this).html().replace(new RegExp(find_value, 'ig'), function (match) {
                                return '<span class="yellow">' + match + '</span>';
                            }));
                        });
                    } else {
                        $(".popup-lists-find-container").html('');
                        find_container.show();
                    }
                } else {
                    $(".popup-lists-find-container").html('').removeClass('yellow');
                    find_container.show();
                }
            }
        }

        if ($(e.target).attr("class") == "popup-contacts-find") {
            if (e.keyCode != 13 && e.keyCode != 27) {
                var find_value = $(".popup-contacts-find").val(),
                    find_container = $('.popup-contacts .contacts-wrapper:contains("' + find_value + '")');

                if (find_value) {
                    if (find_container.html()) {
                        find_block = '';
                        find_container.each(function () {
                            find_block += '<div class="contacts-wrapper">' + $(this).html() + '</div>';
                        });
                        $(".popup-contacts-find-container").html(find_block);
                        find_container.hide();

                        $(".popup-contacts-find-container .contacts-id-c").each(function () {
                            $(this).html($(this).html().replace(new RegExp(find_value, 'ig'), function (match) {
                                return '<span class="yellow">' + match + '</span>';
                            }));
                        });
                        $(".popup-contacts-find-container .contacts-tel-format1").each(function () {
                            $(this).html($(this).html().replace(new RegExp(find_value, 'ig'), function (match) {
                                return '<span class="yellow">' + match + '</span>';
                            }));
                        });
                        $(".popup-contacts-find-container .contacts-tel-format2").each(function () {
                            $(this).html($(this).html().replace(new RegExp(find_value, 'ig'), function (match) {
                                return '<span class="yellow">' + match + '</span>';
                            }));
                        });
                    } else {
                        $(".popup-contacts-find-container").html('');
                        find_container.show();
                    }
                } else {
                    $(".popup-contacts-find-container").html('').removeClass('yellow');
                    find_container.show();
                }
            }
        }
    });

    $(document).on(EV, function (e) {
        e = e || window.event;
        if ($(e.target).closest('.towns-select').length == 0) {
            if ($(e.target).prop("name") != "nasel_punkt[]" && $(e.target).prop("class") != "option") {
                var name = "towns";
                $("." + name + "-filter .views-widget-filter-nasel_punkt").hide();
                $("." + name + "-filter-kart .views-widget-filter-nasel_punkt").hide();

                setPlaceholdersCities(name, 'Нас. пункт');
            }
        }

        if ($(e.target).closest('.microraion-filter').length == 0) {
            $('.microraion-filter').hide();
        }

        if ($(e.target).closest('.stations-filter').length == 0) {
            $('.stations-filter').hide();
        }

        if ($(e.target).closest('.uslovie-prodazhi-select').length == 0) {
            if ($(e.target).prop("name") != "uslovie_prodazhi[]" && $(e.target).prop("class") != "option") {
                var name = "uslovie-prodazhi";
                $("." + name + "-filter .views-widget-filter-uslovie_prodazhi").hide();

                setPlaceholders(name, 'Условия продажи');
            }
        }

        if ($(e.target).closest('.san-uzel-select').length == 0) {
            if ($(e.target).prop("name") != "san_uzel[]" && $(e.target).prop("class") != "option") {
                var name = "san-uzel";
                $("." + name + "-filter .views-widget-filter-san_uzel").hide();

                setPlaceholders(name, '');
            }
        }

        if ($(e.target).closest('.planirovka-select').length == 0) {
            if ($(e.target).prop("name") != "planirovka[]" && $(e.target).prop("class") != "option") {
                var name = "planirovka";
                $("." + name + "-filter .views-widget-filter-planirovka").hide();

                setPlaceholders(name, '');
            }
        }

        if ($(e.target).closest('.tip-doma-select').length == 0) {
            if ($(e.target).prop("name") != "tip_doma[]" && $(e.target).prop("class") != "option") {
                var name = "tip-doma";
                $("." + name + "-filter .views-widget-filter-tip_doma").hide();

                setPlaceholders(name, '');
            }
        }

        if ($(e.target).closest('.remont-select').length == 0) {
            if ($(e.target).prop("name") != "remont[]" && $(e.target).prop("class") != "option") {
                var name = "remont";
                $("." + name + "-filter .views-widget-filter-remont").hide();

                setPlaceholders(name, '');
            }
        }

        if ($(e.target).closest('.agenstva-select').length == 0) {
            if ($(e.target).prop("name") != "agenstva[]" && $(e.target).prop("class") != "option") {
                var name = "agenstva";
                $("." + name + "-filter .views-widget-filter-agenstva").hide();

                setPlaceholders(name, 'Любое агентство');
            }
        }

        if ($(e.target).closest('.popup-contacts-menu').length == 0) {
            if ($(e.target).prop("class") != "contacts-button") {
                $(".popup-contacts-menu").hide();
            }
        }

        if ($(e.target).closest('.my_data_contacts_wrap').length == 0) {
            if ($(e.target).prop("id") != "select_contacts") {
                $(".my_data_contacts_wrap").hide();
            }
        }


        if ($(e.target).closest('.popup-lists-menu').length == 0) {
            if ($(e.target).prop("class") != "ad-add-my-list" && $(e.target).prop("class") != "ads-add-my-list") {
                $(".popup-lists-menu").hide();
                $(".popup-lists-find").val('');
            }
        }

        if ($(e.target).closest('.ufield-myads-lists .list-popup').length == 0) {
            if ($(e.target).prop("class") != "list-item") {
                $(".ufield-myads-lists .list-popup").hide();
            }
        }

        if ($(e.target).closest('.sidebar-adv-menu').length == 0) {
            if ($(e.target).prop("class") != "sidebar-arrow") {
                $('.sidebar-adv-menu').hide();
                $('.sidebar-arrow').hide();
                if ($(e.target).hasClass("sidebar-list-wrapper")) {
                    $(e.target).find('.sidebar-arrow').show();
                }
                $(".sidebar-list-wrapper").removeClass("menu-active")
            }
        }

        if ($(e.target).closest('.popup-dop').length == 0) {
            if ($(e.target).prop("class") != "dop-button") {
                $('.popup-dop').hide();
            }
        }

        if ($(e.target).closest('.contacts-menu-box').length == 0) {
            if ($(e.target).prop("class") != "contacts-arrow") {
                $(".contacts-arrow").hide();
                $(".contact-wrapper").css("background", "");
                $(".contacts-menu-box").hide();
            }
        }
    });

    $(document).ready(function ($) {
        mainUrl = $.url(document.location.href);

        $('.nav-toggle').click(function () {
            $('#main-menu div ul:first-child').slideToggle(250);
            return false;
        });

        if (($(window).width() > 640) || ($(document).width() > 640)) {
            $('#main-menu li').mouseenter(function () {
                $(this).children('ul').css('display', 'none').stop(true, true).slideToggle(250).css('display', 'block').children('ul').css('display', 'none');
            });
            $('#main-menu li').mouseleave(function () {
                $(this).children('ul').stop(true, true).fadeOut(250).css('display', 'block');
            })
        } else {
            $('#main-menu li').each(function () {
                if ($(this).children('ul').length)
                    $(this).append('<span class="drop-down-toggle"><span class="drop-down-arrow"></span></span>');
            });
            $('.drop-down-toggle').click(function () {
                $(this).parent().children('ul').slideToggle(250);
            });
        }

        $.fn.center = function () {
            this.css({
                'position': 'fixed',
                'left': '50%',
                'top': '50%'
            });
            this.css({
                'margin-left': -this.outerWidth() / 2 + 'px',
                'margin-top': -this.outerHeight() / 2 + 'px'
            });

            return this;
        };

        $('body').on('click', '.admin-menu-account', function (e) {
            e.preventDefault();

            document.location.href = 'user/' + user_id + '/edit?destination=data';
        });

        if (cur_page == "congratulations") {
            $(".user-register-redirect").on('click', function () {
                document.location.href = 'user';
            });

            $("#wrapper").css("background", "#ffffff");
            $(".container").css("min-height", "500px");
        }

        if (cur_page == "user") {
            $('body').on('click', '.user-profile-buttons', function () {
                document.location.href = '/data';
            });

            $("#wrapper").css("background", "#ffffff");
            $(".container").css("min-height", "500px");

            $("#user-profile-form #edit-submit").click(function () {
                $(this).parent("#edit-actions").append(loader);
            });

            $("#user-pass-reset #edit-submit").click(function () {
                $(this).parent("#edit-actions").append(loader);
            });
            $("#user-register-form .form-submit").click(function () {
                $(this).parent("#edit-actions").append(loader);
            });

            $("#user-register-form .form-item-name label").html('Придумайте имя пользователя <span class="form-required" title="Это поле обязательно для заполнения.">*</span>');
            $("#user-register-form #edit-field-mobile-phone-und-0-value").mask('+375?999999999');
            $("#user-profile-form #edit-field-mobile-phone-und-0-value").mask('+375?999999999');
        }
// общие события для всех страниц
        // anti spambot
        var mailLabel = $('a:contains("{mail1}")');
        if (mailLabel.length > 0) {
            var mail1 = "support@rea.by";
            mailLabel.html(mailLabel.html().replace(/{mail1}/ig, mail1));
            mailLabel.attr("href", "mailto:" + mail1);
        }

        if (is_admin) {
            setTimeout(function () {
                $(".admin-menu-icon").show();

            }, 800);
        }

        // Copyright footer current year
        $(".last-year").text(cYear);

        // Редирект на форму входа, если нет доступа к странице
        if ((cur_page == "preview") && !logged_in) {
            window.close();
        }

        if (cur_page !== "help") {
            if ((cur_page !== "user" && cur_page !== "congratulations") && !logged_in) {
                document.location.href = 'user';
            }
            if ((cur_page == "user") && !logged_in) {
                if (typeof(cur_page2) !== "undefined") {
                    if (cur_page2 == "edit") {
                        document.location.href = 'user';
                    }
                }
            }
        }
        //

// общие функции, события для data и preview
        if ((cur_page == "data" || cur_page == "preview") && logged_in) {
            var openPopupListDialog = function (is_create, listname) {
                $(".overlay-main").show();
                if (is_create) {
//                    $(".new-list-val").val('');
                    $(".new-list-val").val($(".popup-lists-find").val());
                    $(".popup-lists-add").toggle();
                    setTimeout(function () {
                        $(".new-list-val").focus();
                    }, 10);
                } else {
                    $(".edit-list-val").val(listname);
                    $(".popup-lists-edit").toggle();
                    setTimeout(function () {
                        $(".edit-list-val").focus();
                    }, 10);
                }
            }
            var openPopupDialogBox = function (header, msg) {
                $(".dialog-box .header-dialog").html(header);
                $(".dialog-box label").html(msg);
                $(".overlay-main").show();
                $(".dialog-box").toggle();
            }

            $("body").on('click', '.dop-print-kart', function (e) {
                $(".popup-dop").hide();
                res_flag = false;
                var checked_row = $('.check-row-ads-count input[type=checkbox]:checked').length;
                var all_row = $('.check-row-ads-count input[type=checkbox]').length;
                $('input[name=rb_selected_ads]').on('change', function () {
                    if ($(this).parent('div').hasClass('print_selected_ads') && checked_row > 0) {
                        $('.pages_counter').hide(1);
                    }
                    else {
                        $('.pages_counter').show(1);
                    }
                });
                if ($('.dop-button').attr('id') == 'print_one_ad') {
                    $('.print_selected_ads_count').text('Выбранные объявления (1)');
                    $('.print_selected_ads input').prop('checked', true);
                    $('.print_selected_ads_count').removeClass('disabled_rb');
                }
                else if (checked_row > 0) {
                    $('.print_selected_ads_count').text('Выбранные объявления (' + checked_row + ')');
                    $('.print_selected_ads input').prop('checked', true);
                    $('.print_selected_ads_count').removeClass('disabled_rb');
                    $('.print_selected_ads input').off(EV);
                }
                else {
                    $('.print_selected_ads_count').text('Выбранные объявления (0)');
                    $('.print_selected_ads_count').addClass('disabled_rb');
                    $('.print_all_ads input').prop('checked', true);
                    $('.print_selected_ads input').on(EV, function (e) {
                        return false;
                    });
                    $('.pages_counter').show(1);
                }
                $('#popup-kart-print_wrapper').show(1, function () {
                    //$('.print_all_ads_count').text('Все объявления '+'('+$('.main-count-query').text()+')'); 
                    $('.print_all_ads_count').text('Все объявления ' + '(' + all_row + ')');
                    $('.close_print_popup, .print_cancel').on(EV, function () {
                        $('#popup-kart-print_wrapper, .pages_counter').hide(1);
                    });
                    $('.to_print').off().on(EV, function () {
                        if ($('div.ads-content[sort]').length > 0) {
                            var order_params = $('div.ads-content').attr('sort');
                        }
                        else {
                            var order_params = 'default';
                        }
                        if ($('.print_selected_ads input[type=radio]:checked').length > 0) {
                            var karts_id = '';
                            all_print_element = '';
                            $('.check-row-ads-count input[type=checkbox]:checked').each(function (index) {
                                if (index == checked_row - 1) {
                                    karts_id += ("'" + $(this).parent('td').siblings('.hide-field').find('.unid-field').text() + "'");
                                }
                                else {
                                    karts_id += ("'" + $(this).parent('td').siblings('.hide-field').find('.unid-field').text()) + "'" + ',';
                                }
                            });
                            search_params = [];
                            sort_params = [];
                            sidebar_list_id = 0;
                            initPaginationVars(true);
                            setSearchFilters(true);
                            var where_params = JSON.stringify(search_params);

                            $.ajax({
                                url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                                type: 'POST',
                                data: {
                                    method: 'getAdsToPrint',
                                    kart_id: karts_id,
                                    cur_where_params: where_params,
                                    cur_order_params: order_params
                                },
                                async: false,
                                dataType: 'json',
                                beforeSend: function (xhr) {
                                    // $(document.body).append(loader);
                                },
                                success: function (res) {
                                    element_to_print = $('<div></div>').html(res);
                                    //res_flag = true;
                                    setTimeout(function () {
                                        $(element_to_print).printArea({
                                            mode: "popup",
                                            popHt: $(window).height(),
                                            popWd: $(window).width(),
                                            popX: 0,
                                            popY: 0,
                                            popClose: false
                                        });
                                    }, 500);
                                }
                            }).always(function () {
                                //loader.remove();
                            });
                        }
                        else if ($('.print_all_ads input[type=radio]:checked').length > 0) {
                            var karts_id = '';
                            all_print_element = '';
                            $('.check-row-ads-count input[type=checkbox]').each(function (index) {
                                if (index == all_row - 1) {
                                    karts_id += ("'" + $(this).parent('td').siblings('.hide-field').find('.unid-field').text() + "'");
                                }
                                else {
                                    karts_id += ("'" + $(this).parent('td').siblings('.hide-field').find('.unid-field').text()) + "'" + ',';
                                }
                            });
                            search_params = [];
                            sort_params = [];
                            sidebar_list_id = 0;
                            initPaginationVars(true);
                            setSearchFilters(true);
                            var where_params = JSON.stringify(search_params);

                            $.ajax({
                                url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                                type: 'POST',
                                data: {
                                    method: 'getAdsToPrint',
                                    kart_id: karts_id,
                                    cur_where_params: where_params,
                                    cur_order_params: order_params
                                },
                                async: false,
                                dataType: 'json',
                                beforeSend: function (xhr) {
                                    // $(document.body).append(loader);
                                },
                                success: function (res) {
                                    element_to_print = $('<div></div>').html(res);
                                    //res_flag = true;
                                    setTimeout(function () {
                                        $(element_to_print).printArea({
                                            mode: "popup",
                                            popHt: $(window).height(),
                                            popWd: $(window).width(),
                                            popX: 0,
                                            popY: 0,
                                            popClose: false
                                        });
                                    }, 500);
                                }
                            }).always(function () {
                                //loader.remove();
                            });
                        }
                        else {
                            return false;
                        }
                        $('#popup-kart-print_wrapper').hide(1);
                    });
                });
            });

            $('body').on('click', '.dialog-cancel', function () {
                $(".overlay-main").hide();
                $(".dialog-box").hide();
            });

            $('body').on('change', '.popup-lists-menu .list-wrapper input:checkbox', function () {
                $(".list-action a").addClass("apply-list").text('Применить');
            });

            $('body').on('mousedown', '.sidebar-control-open', function () {
                $(this).hide();

                // подгружаем в sidebar все списки пользователя
                getListsSidebar();
                $(".sidebar-block").show();
            });

            $('body').on('mousedown', '.sidebar-control-close', function () {
                $(".sidebar-block").hide();
                $(".sidebar-control-open").show();
            });
        }

        if (cur_page == "data" && logged_in) {
            var getKartAndListInfoGroup = function () {
                $.ajax({
                    url: 'realty/realty/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getKartAndListInfoGroup',
                        cur_unids: JSON.stringify(unids_in_page)
                    },
                    beforeSend: function (xhr) {
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            if (response.data) {
                                var info_data = response.data;

                                info_data.forEach(function (val, i) {
                                    var unid1 = val['unid'],
                                        lists = val['lists'],
                                        kart_id = val['kart_id'];

                                    if (lists) {
                                        var arr_lists = lists.split('----::----'),
                                            container = $('.unid-field:contains("' + unid1 + '")');

                                        container.parent().siblings(".kart-row-ads").find(".kart-label").hide();
                                        container.parent().siblings(".kart-row-ads").find(".lists-block").html('');

                                        cur_unid_lists = '';
                                        arr_lists.forEach(function (v, ind) {
                                            if (v != 'Корзина') {
                                                cur_unid_lists += '<span class="list-item">' + v + '</span>';
                                                $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".lists-block").show();
                                            } else {
                                                $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".lists-block").hide();
                                                $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".rec-label").show();
                                            }
                                        });

                                        if (cur_unid_lists != '') {
                                            $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".lists-block").append(cur_unid_lists);
                                        }
                                    } else {
                                        if (kart_id) {
                                            $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".rec-label").hide();
                                            $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".kart-label").show();
                                        }
                                        $('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads").find(".lists-block").hide();
                                    }
                                });
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    //                        loader.remove();
                });
            };
            var getYellowMessageGroup = function (current_unids) {
                $.ajax({
                    url: 'realty/realty/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getYellowMessageGroup',
                        cur_unids: current_unids
                    },
                    beforeSend: function (xhr) {
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            if (response.data) {
                                var info_data = response.data;

                                if (info_data.length > 0) {
                                    $('body.page-data .header2 .toggle-contacts-exists').css({"display": "inline-block"});
                                    $('body.page-data .header2 .toggle-contacts-exists-span').css({"display": "inline-block"});
                                }
                                else {
                                    $('body.page-data .header2 .toggle-contacts-exists').css({"display": "none"});
                                    $('body.page-data .header2 .toggle-contacts-exists-span').css({"display": "none"});
                                }

                                $('.unid-field').parent().siblings(".total-ads-count").removeClass("contacts-exists");
                                info_data.forEach(function (val, i) {
                                    $('.unid-field:contains("' + val + '")').parent().siblings(".total-ads-count").addClass("contacts-exists");
                                });
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    //                        loader.remove();
                });
            };
            var showKartLabel = function (sw) {
                if (sw == 'trash') {
                    unids.forEach(function (val, i) {
                        var container = $('.unid-field:contains("' + val['unid'] + '")');

                        container.parent().siblings(".kart-row-ads").find(".rec-label").show();
                        container.parent().siblings(".kart-row-ads").find(".kart-label").hide();
                        container.parent().siblings(".kart-row-ads").find(".lists-block").hide();
                    });
                } else {
                    unids.forEach(function (val, i) {
                        var container = $('.unid-field:contains("' + val['unid'] + '")');

                        container.parent().siblings(".kart-row-ads").find(".rec-label").hide();
                        container.parent().siblings(".kart-row-ads").find(".kart-label").show();
                        if (container.parent().siblings(".kart-row-ads").find(".lists-block").is(":visible")) {
                            container.parent().siblings(".kart-row-ads").find(".kart-label").hide();
                        } else {
                            container.parent().siblings(".kart-row-ads").find(".kart-label").show();
                        }
                    });
                }
            };
            var showAdList = function (sw) {
                unids.forEach(function (val, i) {
                    var container = $('.unid-field:contains("' + val['unid'] + '")');

                    container.parent().siblings(".kart-row-ads").find(".kart-label").hide();
                    container.parent().siblings(".kart-row-ads").find(".lists-block").show().append('<span class="list-item">' + sw + '</span>');
                });
            };
            var setKartAndContactsGroup = function (current_unids) {
                $.ajax({
                    url: 'realty/realty/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_setKartAndContactsGroup',
                        cur_unids: current_unids
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            showKartLabel();

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            $(".unid-field").each(function () {
                                unids_in_page.push({
                                    unid: $(this).text()
                                });
                            });

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакт с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        } else {
                            $('.ads-add-my').show();

                            if (response.status == 'logout') {
                                user_logout();
                            }

                            if (response.status == 'maintenance') {
                                user_maintenance();
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var getListsGroup = function (ads) {
                $(".popup-lists-menu").toggle();

                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getListsGroup',
                        cur_ads: ads
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".popup-lists-find-container").html('');
                            $(".popup-lists-menu .popup-lists").empty().append('<div>' + response.data1 + '</div>'
                                + '<div>' + response.data2 + '</div>'
                                + '<div>' + response.data3 + '</div>');

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакты с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids));
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var getListsNamesGroup = function () {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getListsNamesGroup'
                    },
                    beforeSend: function (xhr) {
//                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".popup-lists-find-container").html('');
                            $(".popup-lists-menu .popup-lists").empty().append('<div>' + response.data1 + '</div>');
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
//                        loader.remove();
                });
            };
            var getListsSidebar = function () {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getListsSidebar'
                    },
                    beforeSend: function (xhr) {
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".sidebar-container").html(response.data);
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                });
            };
            var createNewListGroup = function (ads, new_list_name) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_createNewListGroup',
                        cur_ads: ads,
                        list_name: new_list_name
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".ads-add-my").hide();
                            showKartLabel();
                            showAdList(new_list_name);

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                            getListsNamesGroup();

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакты с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        } else {
                            if (response.message) {
                                alert(response.message);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var createNewListName = function (new_list_name) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'createNewListName',
                        list_name: new_list_name
                    },
                    beforeSend: function (xhr) {
                        $('.header2').prepend(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                            getListsNamesGroup();
                        } else {
                            if (response.message) {
                                alert(response.message);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var renameList = function (new_list_name, list_id) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'renameList',
                        cur_list_id: list_id,
                        list_name: new_list_name
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            // выставляем для каждой записи мк
                            getKartAndListInfoGroup();

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                            getListsNamesGroup();
                        } else {
                            if (response.message) {
                                alert(response.message);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var deleteList = function (list_id) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'deleteList',
                        cur_list_id: list_id
                    },
                    beforeSend: function (xhr) {
//                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            // выставляем для каждой записи мк
                            getKartAndListInfoGroup();

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                            getListsNamesGroup();
                        } else {
                            if (response.message) {
                                alert(response.message);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
//                        loader.remove();
                });
            };
            var moveToTrashGroup = function (ads) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_moveToTrashGroup',
                        cur_ads: ads
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            showKartLabel('trash');

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                            getListsNamesGroup();

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакты с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var setKartListsGroup = function (ads, list_ids) {
                $.ajax({
                    url: 'realty/realty/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_setKartListsGroup',
                        cur_ads: ads,
                        list_ids: list_ids
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader)
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            showKartLabel();

                            // выставляем для каждой записи мк
                            getKartAndListInfoGroup();

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакты с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var setAllCheckAdsCount = function () {
                var ads_checked = $(".tab-ads:visible .check-row-ads-count input:checkbox:checked");
                $(".ads-check-count").text((ads_checked.length > 0) ? ads_checked.length : '');
            };
            var pushAdToArrAds = function (cur_unid) {
                unid_in_array = 0;
                unids.forEach(function (val, i) {
                    if (val['unid'] == cur_unid) {
                        unid_in_array = 1;
                    }
                });

                if (unid_in_array == 0) {
                    unids.push({
                        unid: cur_unid
                    });
                }
                setAllCheckAdsCount();
            };
            var removeAdFromArrAds = function (cur_unid) {
                for (var i = 0; i < unids.length; i++) {
                    if ((unids[i]).unid == cur_unid) {
                        unids.splice(i, 1);
                    }
                }
                setAllCheckAdsCount();
            };
            var toggleButton = function (selector_name, trash_button) {
                var addMy = 0,
                    trash = 0;

                if (selector_name.find(".lists-block").is(":visible")) {
                    addMy++;
                }
                if (selector_name.find(".kart-label").is(":visible")) {
                    addMy++;
                }
                if (selector_name.find(".rec-label").is(":visible")) {
                    addMy++;
                    trash++;
                }

                (addMy > 0) ? $(".ads-add-my").hide() : $(".ads-add-my").show();
                if (trash_button) {
                    (trash > 0) ? $(".ads-add-my-rec").hide() : $(".ads-add-my-rec").show();
                }
            };
            var isKartExists = function (selector_name) {
                var addMy = 0;

                if (selector_name.find(".lists-block").is(":visible")) {
                    addMy++;
                }
                if (selector_name.find(".kart-label").is(":visible")) {
                    addMy++;
                }
                if (selector_name.find(".rec-label").is(":visible")) {
                    addMy++;
                }

                return (addMy > 0) ? true : false;
            };
            var removeKartGroup = function (ads) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_removeKartGroup',
                        cur_unids: ads
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".popup-dop").hide();
                            $(".ads-add-my").show();

                            unids.forEach(function (val, i) {
                                var container = $('.unid-field:contains("' + val['unid'] + '")');

                                container.parent().siblings(".kart-row-ads").find('.lists-block').html('');
                                container.parent().siblings(".kart-row-ads").find('.kart-label').hide();
                                container.parent().siblings(".kart-row-ads").find('.rec-label').hide();
                            });

                            // подгружаем в sidebar все списки пользователя
                            getListsSidebar();
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var getMainAdsData = function (where_params, order_params) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getMainAdsData',
                        cur_where_params: where_params,
                        cur_order_params: order_params
                    },
                    beforeSend: function (xhr) {
                        $(".header2").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".ads-content").html(response.data).show();

                            // Перейти вверх
                            window.scroll(0, 0);

                            // групповое выделения объявлений
                            unids = [];
                            search_params = [];
                            unids_in_page = [];

                            $(".ads-check-count").text('');
                            $(".ads-check-all").prop("checked", false);
                            //$(".ads-buttons-my").hide();
                            $(".ads-buttons-my").show(1);
                            $(".info-block-empty").hide();
                            $(".empty-query-message").hide();
                            $(".pager-ads, .sidebar-control-open, .ads-buttons").show();
                            $(".counter-page, .sidebar-control-open, .ads-buttons").show();
                            $(".dop-button, .ads-check-count, .ads-check-all").show(1);
                            ($(".sidebar-control-close").is(":visible")) ? $(".sidebar-control-open").hide() : $(".sidebar-control-open").show();
                            ($(".tab-ads:visible input:checkbox").length > 0) ? $(".ads-buttons, .ads-check-all, .ads-buttons img").show() :
                                $(".ads-buttons, .ads-check-all, .ads-buttons img").hide();
                            if ($.cookie("main-unid") !== undefined) {
                                var cur_unid = $.cookie("main-unid");
                                $(".tab-ads").find(".unid-field:contains(" + cur_unid + ")").parents("tr").addClass("highlight-ad");

//                                if ($(".highlight-ad").length > 0) {
//                                    // set center position
//                                    window.scroll(500,($(".highlight-ad").position().top - 300));
//                                }
                            }
                            // page counter
                            if ($(".main-count-query").text() != 0) {
                                $(".pager-ads .pager-ads-count").html(' из <span>' + $(".main-count-query").text() + '</span>' + ')');
                            }

                            max_count = $(".pager-ads .pager-ads-count span").text();
                            var max_page = (parseInt(offset_page) + parseInt(limit_page));

                            if (max_page >= max_count) {
                                max_page = max_count;
                            }

                            $(".counter-page").text('(' + offset_page + ' - ' + max_page);
                            //

                            $(".unid-field").each(function () {
                                unids_in_page.push({
                                    unid: $(this).text()
                                });
                            });

                            // выставляем для каждой записи мк
                            getKartAndListInfoGroup();

                            // отрисовываем серые кнопки пагинации
                            printPaginationButtons();

                            // выставляем пометку о том, что в связанных объявлениях уже имеется контакты с тел1,тел2
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        }

                        if (response.status == 'error') {
                            $(".ads-content, .pager-ads, .sidebar-control-open, .ads-buttons").hide();
                            $(".info-block-empty").show();
                            $(".empty-query-message").show();
                        }

                        if (response.status == 'logout') {
                            user_logout();
                        }

                        if (response.status == 'maintenance') {
                            user_maintenance();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };
            var initPaginationVars = function (reset) {
                if (!reset) {
                    page1 = (typeof(mainUrl.param()['page']) == "undefined") ? 1 : mainUrl.param()['page'];
                    limit1 = (typeof(mainUrl.param()['limit']) == "undefined") ? $.trim($(".limit-control option:selected").val()) : mainUrl.param()['limit'];
                    offset1 = (typeof(mainUrl.param()['offset']) == "undefined") ? 0 : mainUrl.param()['offset'];

                    if (typeof(main_page) === "undefined") {
                        main_page = page1;
                        next_page = main_page;
                        limit_page = limit1;
                        offset_page = offset1;
                    }
                } else {
                    main_page = 1;
                    next_page = main_page;
                    limit_page = $.trim($(".limit-control option:selected").val());
                    offset_page = 0;
                }
            };
            var setSearchFilters = function (use_count_query) {
                var towns_arr = function () {
                    var arr = [];
                    $(".towns-filter input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var raion_arr = function () {
                    var arr = [];
                    $(".raion-filter .raion-input input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var microraion_arr = function () {
                    var arr = [];
                    $(".microraion-filter input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var metrolines_arr = function () {
                    var arr = [];
                    $(".liniya-metro-filter .line-input input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var metrostations_arr = function () {
                    var arr = [];
                    $(".stations-filter input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var komnat_arr = function () {
                    var arr = [];
                    $(".views-widget-filter-obchee_chislo_komnat input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var sanuzel_arr = function () {
                    var arr = [];
                    $(".views-widget-filter-san_uzel input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var planirovka_arr = function () {
                    var arr = [];
                    $(".views-widget-filter-planirovka input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var tip_doma_arr = function () {
                    var arr = [];
                    $(".views-widget-filter-tip_doma input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var remont_arr = function () {
                    var arr = [];
                    $(".views-widget-filter-remont input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var istochnik_informacii_arr = function () {
                    var arr = [];
                    $(".istochnik-informacii-filter input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var status_prodavca_arr = function () {
                    var arr = [];
                    $(".status-prodavca-filter input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var uslovie_prodazhi_arr = function () {
                    var arr = [];
                    $(".form-item-uslovie_prodazhi input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };
                var agenstva_arr = function () {
                    var arr = [];
                    $(".form-item-agenstva input:checkbox:checked").each(function () {
                        arr.push($(this).val());
                    });

                    return arr;
                };

                search_params.push({
                    list_id: sidebar_list_id,
                    limit: limit_page,
                    offset: offset_page,
                    count: use_count_query,
                    towns: towns_arr(),
                    ulica: $.trim($(".views-widget-ulica input").val()),
                    nomer_doma: $.trim($(".views-widget-nomer-doma input").val()),
                    raion: raion_arr(),
                    microraion: microraion_arr(),
                    metro_lines: metrolines_arr(),
                    metro_stations: metrostations_arr(),
                    cena_min: $.trim($("#edit-cena-min").val()),
                    cena_max: $.trim($("#edit-cena-max").val()),
                    komnat: komnat_arr(),
                    obchaja_plochad_min: $.trim($("#obchaja-plochad-min").val()),
                    obchaja_plochad_max: $.trim($("#obchaja-plochad-max").val()),
                    plochad_zhilaja_min: $.trim($("#plochad-zhilaja-min").val()),
                    plochad_zhilaja_max: $.trim($("#plochad-zhilaja-max").val()),
                    plochad_kuhni_min: $.trim($("#plochad-kuhni-min").val()),
                    plochad_kuhni_max: $.trim($("#plochad-kuhni-max").val()),
                    etazh_min: $.trim($("#etazh-min").val()),
                    etazh_max: $.trim($("#etazh-max").val()),
                    etazhnost_min: $.trim($("#etazhnost-min").val()),
                    etazhnost_max: $.trim($("#etazhnost-max").val()),
                    god_postrojki_min: $.trim($("#god-postrojki-min").val()),
                    god_postrojki_max: $.trim($("#god-postrojki-max").val()),
                    nalichie_lodzhii: ($(".views-widget-filter-nalichie_lodzhii input:checkbox").prop("checked")) ? 1 : 0,
                    god_cap_remonta: ($(".views-widget-filter-god_cap_remonta input:checkbox").prop("checked")) ? 1 : 0,
                    sanuzel: sanuzel_arr(),
                    planirovka: planirovka_arr(),
                    tip_doma: tip_doma_arr(),
                    remont: remont_arr(),
                    data_postupleniya_min: $.trim($("#edit-data-postupleniya-min").val()),
                    data_postupleniya_max: $.trim($("#edit-data-postupleniya-max").val()),
                    data_postupleniya_offset: $.trim($(".data-postupleniya-offset-filter option:selected").val()),
                    archiv: $.trim($(".archiv-elements option:selected").val()),
                    istochnik_informacii: istochnik_informacii_arr(),
                    status_prodavca: status_prodavca_arr(),
                    uslovie_prodazhi: uslovie_prodazhi_arr(),
                    agenstva: agenstva_arr(),
                    tel: $.trim($(".tel-filter .tel-find").val())
                });
            };
            var setSortFilters = function (field, sort_direction) {
                var record = {};

                record[field] = true;
                sort_params.push(record, sort_direction);
            };
            var printPaginationButtons = function () {
                if (next_page == 1) {
                    $(".pager-ads .go-back-page").css("color", "#ccc");
                } else {
                    $(".pager-ads .go-back-page").css("color", "#000");
                }

                if (parseInt(offset_page) + parseInt(limit_page) >= max_count) {
                    $(".pager-ads .go-next-page").css("color", "#ccc");
                } else {
                    $(".pager-ads .go-next-page").css("color", "#000");
                }
            };
            var resetSearchForm = function () {
                // set default values to all fearch form elements
                $(".custom-filters input[type='text']").val('');
                $(".custom-filters input:checkbox:checked").prop("checked", false);
                $(icon_boxes).hide();
                $(".limit-control option[value=50]").prop("selected", true);
                $(".istochnik-informacii-filter input:checkbox[value!=5]").prop("checked", true);
                $(".views-widget-filter-nasel_punkt input:checkbox[value=1]").prop("checked", true);
                $(".archiv-elements option:selected").prop("selected", false);
                $(".archiv-elements option:first").prop("selected", true);
                $(".date-offset-elements option:first").prop("selected", true);
                $(document).click();
            };

// data events
            $('body').on('click', '.toggle-contacts-exists', function () {
                ($(".tab-ads:visible input:checkbox").length > 0) ? $(".ads-buttons, .ads-check-all, .ads-buttons img").show() :
                    $(".ads-buttons, .ads-check-all, .ads-buttons img").hide();
            });

            $('body').on('change', '.toggle-contacts-exists', function () {
                unids = [];
                $(".contacts-exists").parent().toggle();
            });

            $('body').on('click', '.form-item-data-postupleniya-min input', function () {
                if ($(this).val() != '') {
                    $(".date-offset-elements option:first").prop("selected", true);
                }
            });

            $('body').on('click', '.form-item-data-postupleniya-max input', function () {
                if ($(this).val() != '') {
                    $(".date-offset-elements option:first").prop("selected", true);
                }
            });

            // сортировка таблицы по клику на заголовок
            $('body').on('click', '.tab-ads-headers .sort-header', function () {
                sort_params = [];
                var cur_header = $(this).attr("id");
                var sort_col = '';
                switch (cur_header) {
                    case 'sort_cena':
                        sort_col = 'cena';
                        break;
                    case 'sort_ulica':
                        sort_col = 'ulica';
                        break;
                    case 'sort_komnat':
                        sort_col = 'obchee_chislo_komnat'
                        break;
                    case 'sort_date':
                        sort_col = 'data_postupleniya'
                        break;

                    case 'sort_vnutri':
                        sort_col = 'total_ad ';
                        break;

                    case 'sort_photo':
                        sort_col = 'total_photo';
                        break;


                }
                if (cur_header != 'sort_date') {
                    if (sort_field == cur_header) {
                        sort_field = '';
                        var sort_direction = 'ASC';
                        sessionStorage.ascDesc = sort_direction;
                    } else {
                        sort_field = cur_header;
                        var sort_direction = 'DESC';
                        sessionStorage.ascDesc = sort_direction;
                    }
                }
                else {
                    if (sort_field_date == cur_header) {
                        sort_field_date = '';
                        var sort_direction = 'DESC';
                        sessionStorage.ascDesc = sort_direction;
                    } else {
                        sort_field_date = cur_header;
                        var sort_direction = 'ASC';
                        sessionStorage.ascDesc = sort_direction;
                    }
                }
                if (sort_col == 'total_ad ') {
                    if (sort_field_date == cur_header) {
                        sort_field_date = '';
                        var sort_direction = 'DESC';
                        sessionStorage.ascDesc = '';
                        sort_col += sort_direction + ', data_postupleniya DESC';
                    }
                    else {
                        sort_field_date = cur_header;
                        var sort_direction = 'ASC';
                        sessionStorage.ascDesc = '';
                        sort_col += sort_direction + ', data_postupleniya DESC';
                    }
                }
                $('div.ads-content').attr('sort', sort_col + ' ' + sort_direction);
                sessionStorage.sortColToPrint = sort_col;

                // устанавливаем фильтры поиска
                setSearchFilters(false);

                // устанавливаем фильтры сортировки таблицы
                setSortFilters($(this).attr("id"), sort_direction);

                // получаем данные по объявлениям и загружаем их в основную таблицу
                getMainAdsData(JSON.stringify(search_params), JSON.stringify(sort_params));
            });

            // предидущая страница
            $('body').on('click', '.pager-ads .go-back-page', function () {
                if (next_page > 1) {
                    next_page--;
                    offset_page = parseInt(offset_page) - parseInt(limit_page);

                    // устанавливаем фильтры поиска
                    setSearchFilters(false);

                    // получаем данные по объявлениям и загружаем их в основную таблицу
                    getMainAdsData(JSON.stringify(search_params), JSON.stringify(sort_params));
                }
            });

            // след. страница
            $('body').on('click', '.pager-ads .go-next-page', function () {
                if (parseInt(offset_page) + parseInt(limit_page) <= max_count) {
                    next_page++;
                    offset_page = parseInt(limit_page) + parseInt(offset_page);

                    // устанавливаем фильтры поиска
                    setSearchFilters(false);

                    // получаем данные по объявлениям и загружаем их в основную таблицу
                    getMainAdsData(JSON.stringify(search_params), JSON.stringify(sort_params));
                }
            });

            $(".dop-button").on('click', function () {
                $(".popup-dop").toggle();
            });

            $(".popup-dop div").on('mouseover', function () {
                var karts_count = 0;
                unids.forEach(function (val, i) {
                    var unid1 = val['unid'];

                    if (isKartExists($('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads"))) {
                        karts_count++;
                    }
                });

                if ((karts_count == 0) && $(this).hasClass("dop-remove-kart")) {
                    $(this).css("cursor", "default");
                } else {
                    $(this).css("color", "#000000").css("cursor", "pointer");
                }
            });

            $(".popup-dop div").on('mouseleave', function () {
                $(this).css("color", "#B1A9BB");
            });

            $('body').on('mouseover', '.tab-ads', function () {
                $(this).find(".hover-row-ads").css("background", "#F0F0F0");
            });

            $('body').on('mouseleave', '.tab-ads', function () {
                $(this).find(".hover-row-ads").css("background", "");
            });

            $("body").on('click', '.dop-remove-kart', function (e) {
                var del_karts = '',
                    del_karts_count = 0;

                unids.forEach(function (val, i) {
                    var container = $('.unid-field:contains("' + val['unid'] + '")');

                    if (isKartExists(container.parent().siblings(".kart-row-ads"))) {
                        del_karts += '<br />' + '"' + container.parent().siblings('td').find(".address-std").text() + '"';
                        del_karts_count++;
                    }
                });

                if (del_karts_count > 0) {
                    openPopupDialogBox('<h3>Удаление моей недвижимости (' + del_karts_count + ')</h3>',
                        'Удалить: ' + del_karts + ' ? <br /> Контакты не будут удалены.');

                    $(".dialog-ok").off('click').on('click', function () {
                        $(".overlay-main").hide();
                        $(".dialog-box").hide();

                        // Удаление карточек
                        removeKartGroup(JSON.stringify(unids));
                    });
                }
            });

            // sidebar timer
            var timer = 0,
                timeOut = 500;
            $('body').on('mouseover', '.sidebar-list-wrapper', function () {
                if (timer) {
                    clearTimeout(timer);
                }
                (function (that) {
                    timer = setTimeout(function () {
                        that.find(".sidebar-list-id .listname-real").show();
                    }, timeOut);
                })($(this));
            });

            $('body').on('mouseleave', '.sidebar-list-wrapper', function () {
                if (timer) {
                    clearTimeout(timer);
                }
                $(this).find(".sidebar-list-id .listname-real").hide();
            });
            // end sidebar timer

            // переименовать список
            $('body').on('click', '.sidebar-adv-menu .rename-list', function (e) {
                e.preventDefault();

                cur_listid = $(this).parent().siblings(".sidebar-list-id").attr("data-list-id");
                var cur_listname = $(this).parent().siblings(".sidebar-list-id").attr("data-list-name");
                openPopupListDialog(false, cur_listname);
            });

            // удалить список
            $('body').on('click', '.sidebar-adv-menu .delete-list', function (e) {
                e.preventDefault();

                this_list = $(this).parent().parent(".sidebar-list-wrapper");
                cur_listid = $(this).parent().siblings(".sidebar-list-id").attr("data-list-id");
                var cur_listname = $(this).parent().siblings(".sidebar-list-id").attr("data-list-name");

                openPopupDialogBox('<h2>Удаление списка</h2>', 'Удалить "' + cur_listname + '" ? Содержащаяся в нем моя недвижимость не будет удалена.');

                // удаляем обработчик события
                $(".dialog-ok").off('click');

                $(".dialog-ok").one('click', function () {
                    // удаляем наименование из sidebar списка
                    this_list.hide();

                    $(".overlay-main").hide();
                    $(".dialog-box").hide();

                    deleteList(cur_listid);
                });
            });

            // events custom select
            $("body").on('mousedown', '.towns-select', function (e) {
                e.preventDefault();
                $(".towns-filter .views-widget-filter-nasel_punkt").toggle();
            });

            $("body").on('mousedown', '.uslovie-prodazhi-select', function (e) {
                e.preventDefault();
                $(".uslovie-prodazhi-filter .views-widget-filter-uslovie_prodazhi").toggle();
            });

            $("body").on('mousedown', '.san-uzel-select', function (e) {
                e.preventDefault();
                $(".san-uzel-filter .views-widget-filter-san_uzel").toggle();
            });

            $("body").on('mousedown', '.planirovka-select', function (e) {
                e.preventDefault();
                $(".planirovka-filter .views-widget-filter-planirovka").toggle();
            });

            $("body").on('mousedown', '.tip-doma-select', function (e) {
                e.preventDefault();
                $(".tip-doma-filter .views-widget-filter-tip_doma").toggle();
            });

            $("body").on('mousedown', '.remont-select', function (e) {
                e.preventDefault();
                $(".remont-filter .views-widget-filter-remont").toggle();
            });

            $("body").on('mousedown', '.agenstva-select', function (e) {
                e.preventDefault();
                $(".agenstva-filter .views-widget-filter-agenstva").toggle();
            });

            $('[name="uslovie_prodazhi[]"]').on('click', function () {
                if ($(".uslovie-prodazhi-filter input:checkbox:checked").length > 0) {
                    $(".uslovie-prodazhi-box").show();
                } else {
                    $(".uslovie-prodazhi-box").hide();
                }
            });

            $('[name="san_uzel[]"]').on('click', function () {
                if ($(".san-uzel-filter input:checkbox:checked").length > 0) {
                    $(".san-uzel-box").show();
                } else {
                    $(".san-uzel-box").hide();
                }
            });

            $('[name="planirovka[]"]').on('click', function () {
                if ($(".planirovka-filter input:checkbox:checked").length > 0) {
                    $(".planirovka-box").show();
                } else {
                    $(".planirovka-box").hide();
                }
            });

            $('[name="tip_doma[]"]').on('click', function () {
                if ($(".tip-doma-filter input:checkbox:checked").length > 0) {
                    $(".tip-doma-box").show();
                } else {
                    $(".tip-doma-box").hide();
                }
            });

            $('[name="remont[]"]').on('click', function () {
                if ($(".remont-filter input:checkbox:checked").length > 0) {
                    $(".remont-box").show();
                } else {
                    $(".remont-box").hide();
                }
            });

            $('[name="agenstva[]"]').on('click', function () {
                ($(".agenstva-filter input:checkbox:checked").length > 0) ? $(".agenstva-box").show() : $(".agenstva-box").hide();
            });

            $('body').on('mouseover', '.sidebar-list-wrapper', function () {
                if (!$(this).hasClass("menu-active")) {
                    $(this).find(".sidebar-arrow").show();
                }
            });

            $('body').on('mouseleave', '.sidebar-list-wrapper', function () {
                if (!$(this).hasClass("menu-active")) {
                    $(this).find(".sidebar-arrow").hide();
                }
            });

            $('body').on('click', '.sidebar-arrow', function () {
                var that = $(this).siblings(".sidebar-adv-menu"),
                    is_visible = (that.is(":visible"));

                $(".sidebar-adv-menu").hide();
                $(".sidebar-list-wrapper").removeClass("menu-active");

                if (is_visible) {
                    that.hide();
                    $(this).parent().removeClass("menu-active");
                    $(this).hide();
                } else {
                    that.show();
                    $(this).parent().addClass("menu-active");
                    $(this).show();
                }
            });

            // advanced search
            $(".advanced-search").on('click', function () {
                // Перейти вверх
                window.scroll(0, 0);

                $(".views-exposed-form").toggle('slide', 500);

                if ($(".empty-query-message").is(":visible")) {
                    $(".pager-ads, .sidebar-control-open, .ads-buttons, .empty-query-message, .info-block-empty").hide();
                } else {
                    $(".scroll-area-ads, .pager-ads, .ads-buttons, .ads-buttons-my").toggle();
                }
                $(".search-block").toggle();

                ($(".search-block").is(":visible")) ? $(".view-z-flates .view-empty").hide() : $(".view-z-flates .view-empty").show();
            });

// microraions,stations popup
            // click icon-box
            $("body").on('click', icon_boxes, function () {
                uncheckAllCustom($(this));
            });

            // click checkbox
            $('body').on('click', '.microraion-filter .microraion-block', function (e) {
                if ($(e.target).hasClass("microraion-block")) {
                    var chekbox = $(this).find("input:checkbox");
                    chekbox.trigger("click");
                }

                setToggleIconBox($(this), ".microraion-filter");
            });

            $('body').on('click', '.stations-filter .station-block', function (e) {
                if ($(e.target).hasClass("station-block")) {
                    var chekbox = $(this).find("input:checkbox");
                    chekbox.trigger("click");
                }

                setToggleIconBox($(this), ".stations-filter");
            });

            $('body').on('click', '.raion-filter a.raion-name', function () {
                if ($(this).parent().next().next(".microraion-filter").is(":visible")) {
                    $(".microraion-filter").hide();
                    $(this).parent().next().next(".microraion-filter").hide();
                } else {
                    $(".microraion-filter").hide();
                    $(this).parent().next().next(".microraion-filter").show();
                }

                return false;
            });

            $('body').on('click', '.liniya-metro-filter a.line-name', function () {
                if ($(this).parent().next().next(".stations-filter").is(":visible")) {
                    $(".stations-filter").hide();
                    $(this).parent().next().next(".stations-filter").hide();
                } else {
                    $(".stations-filter").hide();
                    $(this).parent().next().next(".stations-filter").show();
                }

                return false;
            });
// end microraions,stations popup

            $(".advanced-search-find").on('click', function () {
                search_params = [];
                sort_params = [];
                sidebar_list_id = 0;

                // переинициализируем переменные для запроса, чтобы изменить url
                initPaginationVars(true);

                // устанавливаем фильтры поиска
                setSearchFilters(true);

                // получаем данные по объявлениям и загружаем их в основную таблицу
                getMainAdsData(JSON.stringify(search_params));

                if ($(".views-exposed-form").is(":visible")) {
                    $(".views-exposed-form").toggle('slide', 500);
                    $(".search-block").toggle();
                }

                if ($(".search-form-ads").is(":visible")) {
//                    $(".advanced-search").trigger('click');
                }
            });

            // применить все фильтры
            $('body').on('click', '.search-form-control-buttons .form-submit', function () {
                search_params = [];
                sort_params = [];
                sidebar_list_id = 0;

                // переинициализируем переменные для запроса, чтобы изменить url
                initPaginationVars(true);

                // устанавливаем фильтры поиска
                setSearchFilters(true);

                // получаем данные по объявлениям и загружаем их в основную таблицу
                getMainAdsData(JSON.stringify(search_params));

                $(".advanced-search").trigger('click');
            });

            $(".search-form-control-buttons .form-reset").on('click', function () {
                // сбросить все фильтры
                resetSearchForm();

                return false;
            });

            $('body').on('click', '.sidebar-create-list', function () {
                sidebar_active = true;
                openPopupListDialog(true, '');
            });

// page data
            search_params = [];
            sort_params = [];
            sort_field = '';
            sort_field_date = '';
            sidebar_list_id = 0;

            // устанавливаем автокомплит на поле "улица"
            $(".views-widget-ulica input").autocomplete({
                source: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc?method=getStreets',
                minLength: 3,
                appendTo: ".views-widget-ulica"
            });

//            // search form
            $(".advanced-search-block").show();

            $(window).load(function () {
                $(".ads-buttons-my, .pager-ads").hide();
                unids = [];

                // сбросить все фильтры
                resetSearchForm();

                // инициализируем переменные для получения данных по объявлениям
                initPaginationVars(false);

                // устанавливаем фильтры поиска
                setSearchFilters(true);

                // получаем данные по объявлениям и загружаем их в основную таблицу
                getMainAdsData(JSON.stringify(search_params));

                // подгружаем в sidebar все списки пользователя
                getListsSidebar();

                // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                getListsNamesGroup();
            });

// data events
            $('body').on('click', '.sidebar-container a', function () {
                search_params = [];
                sidebar_list_id = $(this).parent().attr("data-list-id");

                if ($(".search-form-ads").is(":visible")) {
                    $(".advanced-search").trigger('click');
                }

                // переинициализируем переменные для запроса, чтобы изменить url
                initPaginationVars(true);

                // устанавливаем фильтры поиска
                setSearchFilters(true);

                // получаем данные по объявлениям и загружаем их в основную таблицу
                getMainAdsData(JSON.stringify(search_params));

                return false;
            });

            $(".ads-check-all").on('change', function () {
                // check/uncheck all checkboxes
                $(".tab-ads:visible .check-row-ads input:checkbox").prop("checked", $(this).is(':checked'));

                // if group checked
                if ($(".tab-ads:visible .check-row-ads-count input:checkbox:checked").length > 0) {
                    // Выделяем позицию
                    $(".tab-ads:visible").addClass("selected-ad");

                    $(".ads-buttons-my, .ads-add-my, .ads-add-my-list, .ads-add-my-rec, .dop-button").show();

                    $(".tab-ads:visible .check-row-ads-count input:checkbox:checked").each(function () {
                        var unid1 = $.trim($(this).parents('tr').find('.unid-field').text());

                        unid_in_array = 0;
                        unids.forEach(function (val, i) {
                            if (val['unid'] == unid1) {
                                unid_in_array = 1;
                            }
                        });

                        if (unid_in_array == 0) {
                            unids.push({
                                unid: unid1
                            });
                        }
                    });

                    karts_count = 0;
                    unids.forEach(function (val, i) {
                        var unid1 = val['unid'];

                        if (isKartExists($('.unid-field:contains("' + unid1 + '")').parent().siblings(".kart-row-ads"))) {
                            karts_count++;
                        }
                    });

                    (karts_count > 0) ? $(".ads-add-my").hide() : $(".ads-add-my").show();
                } else {
                    // if group unchecked

                    // Снимаем выделение
                    $(".tab-ads:visible").removeClass("selected-ad");

                    $(".ads-add-my, .ads-add-my-list, .ads-add-my-rec").toggle();
                    $('.ads-add-my').hide(1);
                    $(".tab-ads:visible .check-row-ads-count input:checkbox").each(function () {
                        var unid1 = $.trim($(this).parents('tr').find('.unid-field').text());

                        for (var i = 0; i < unids.length; i++) {
                            if ((unids[i]).unid == unid1) {
                                unids.splice(i, 1);
                            }
                        }
                    });

                }
                setAllCheckAdsCount();
            });

            $('body').on('click', '.status-prodavca-noagent', function () {
                $(".status-prodavca-filter input:checkbox[value=1]").prop("checked", true);
                $(".status-prodavca-filter input:checkbox[value=2]").prop("checked", true);
                $(".status-prodavca-filter input:checkbox[value=3]").prop("checked", true);
            });

            $('body').on('mouseover', '.info-block-icon', function () {
                $(".info-block").show();
            });

            $('body').on('mouseout', '.info-block-icon', function () {
                $(".info-block").hide();
            });

            $('.ads-add-my').on('click', function () {
                $(this).hide();

                setKartAndContactsGroup(JSON.stringify(unids));
            });

            $(".ads-add-my-list").on('click', function () {
                $(".popup-lists-find-container").html('');
                $(".popup-lists-find").val('');

                $(".list-action a").removeClass('apply-list').text('Создать новый список');

                // подгружаем в выпадающее меню кнопки "списки" все списки пользователя
                getListsGroup(JSON.stringify(unids));
            });

            $('body').on('click', '.popup-lists-menu .list-wrapper', function () {
                if ($(this).find("input:checkbox").attr('disabled')) {
                    $(this).find("input:checkbox").attr('disabled', false);
                    $(".list-action a").addClass("apply-list").text('Применить');
                }
            });

            $(".popup-lists-menu a").on('click', function () {
                if ($(this).hasClass("apply-list")) {
                    var list_ids = [],
                        list_names = [];
                    $(".ads-add-my").hide();

                    $(".popup-lists-menu input:checkbox:not(:disabled):visible").siblings(".list-id").each(function () {
                        list_ids.push({
                            list_id: $(this).attr("data-list-id"),
                            remove: !$(this).siblings().prop("checked")
                        });
                    });

                    setKartListsGroup(JSON.stringify(unids), JSON.stringify(list_ids));
                } else {
                    sidebar_active = false;
                    openPopupListDialog(true, '');
                }
                $(".popup-lists-menu").hide();

                return false;
            });

            $(".new-list-ok").on('click', function () {
                if ($.trim($(".new-list-val").val()) != '') {
                    var new_list_name = $.trim($(".new-list-val").val());

                    if (!sidebar_active) {
                        createNewListGroup(JSON.stringify(unids), new_list_name);
                    } else {
                        createNewListName(new_list_name);
                    }
                    sidebar_active = false;
                }

                $(".new-list-cancel").trigger('click');
            });

            $(".edit-list-ok").on('click', function () {
                if ($.trim($(".edit-list-val").val()) != '') {
                    var new_list_name = $.trim($(".edit-list-val").val());

                    renameList(new_list_name, cur_listid);
                }

                $(".edit-list-cancel").trigger('click');
            });

            $(".new-list-cancel").on('click', function () {
                $(".popup-lists-add").hide();
                $(".overlay-main").hide();
            });

            $(".edit-list-cancel").on('click', function () {
                $(".popup-lists-edit").hide();
                $(".overlay-main").hide();
            });

            $(".ads-add-my-rec").on('click', function () {
                $(".ads-add-my").hide();

                moveToTrashGroup(JSON.stringify(unids));
            });

            $('body').on('click', '.tab-ads', function (e) {
                if (e.target.type != 'checkbox' && $(e.target).attr("class") != 'check-row-ads') {
                    var tel1 = $.trim($(this).find('td.tel1-field').text()),
                        tel2 = $.trim($(this).find('td.tel2-field').text()),
                        unid1 = $.trim($(this).find('.unid-field').text());

                    tel1 = (tel1 == '') ? 0 : tel1;
                    tel2 = (tel2 == '') ? 0 : tel2;

                    $.cookie("main-unid", unid1);
                    $(".tab-ads").removeClass("highlight-ad");
                    $(this).addClass("highlight-ad");
//                  исправил открытие в нескольких окнах
                    var params = 'location=no,width=' + ($(window).width()) + ',height=' + ($(window).height()) + ',toolbar=0, menubar=no, resizable=1, scrollbars=yes, scrollbars=1';
                    preview_page = window.open('preview/' + unid1 + '?tel1=' + tel1 + '&tel2=' + tel2 + '&unid=' + unid1, 'windowName'/*'_blank'*/, params);

                    setTimeout(function () {
                        preview_page.focus()
                    }, 250);
                } else {
                    if ($(e.target).attr("class") != 'check-row-ads') {
                        var unid = $.trim($(this).find('.unid-field').text());
                        if ($(e.target).prop("checked")) {
                            // Выделяем позицию светло-желтым
                            $(this).addClass("selected-ad");

                            $(".ads-buttons-my, .ads-add-my-list, .ads-add-my-rec, .dop-button").show();
                            pushAdToArrAds(unid);
                            toggleButton($(this), true);
                        } else {
                            // Снимаем выделение светло-желтым
                            $(this).removeClass("selected-ad");

                            removeAdFromArrAds(unid);
                            if ($(".tab-ads input:checkbox:checked").length == 0) {
                                $(".ads-add-my, .ads-add-my-list, .ads-add-my-rec").hide();
                            }
                        }
                    }
                }
            });


            // Формы контактов при наведении на значок contact-exists
            var pos_cont_form = function (X, Y) {
                var vpY = document.documentElement.clientHeight;
                if (Y + $('.popup-contact-form').height() > vpY) {
                    $('.popup-contact-form').css({
                        "left": X + 5,
                        "top": "unset",
                        "bottom": 0
                    });
                }
                else {
                    $('.popup-contact-form').css({
                        "left": X + 5,
                        "top": Y,
                        "bottom": "unset"
                    });
                }
            }

            var pos_conts_form = function (X, Y) {
                var vpY = document.documentElement.clientHeight;
                if (Y + $('.popup-contacts-form').height() > vpY) {
                    $('.popup-contacts-form').css({
                        "left": X + 5,
                        "top": "unset",
                        "bottom": 0
                    });
                }
                else {
                    $('.popup-contacts-form').css({
                        "left": X + 5,
                        "top": Y,
                        "bottom": "unset"
                    });
                }
            }

            var getContactsByIcon = function (unid, tel1, tel2, c_id, X, Y) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'getContactsByIcon',
                        unid: unid,
                        tel1: tel1,
                        tel2: tel2,
                        c_id: c_id
                    },
                    beforeSend: function (xhr) {
                    },
                    success: function (response) {
                        if (response.type == '1') {
                            $('.popup-contact-form .contact').remove();
                            $('.popup-contact-form .note').remove();
                            $('.popup-contact-form').prepend(response.html);

                            pos_cont_form(X, Y);
                            $('.popup-contact-form').show();
                        }
                        else if (response.type == '2') {
                            $('.popup-contacts-form .contacts').remove();
                            $('.popup-contacts-form').append(response.html);

                            contact_html = response.html1;

                            pos_conts_form(X, Y);
                            $('.popup-contacts-form').show();
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr);
                    },
                    dataType: 'json'
                }).always(function () {
                });
            };

            $('body').on('mouseenter', '.total-ads-count.contacts-exists .icon', function (e) {
                var coord = this.getBoundingClientRect();
                var X = coord.left + 21;
                var Y = coord.top + 21;
                var tel1 = $(this).parents('.tab-ads').find('.tel1-field').text();
                var tel2 = $(this).parents('.tab-ads').find('.tel2-field').text();
                var unid = $(this).parents('.tab-ads').find('.unid-field').text();

                var timeoutInIcon = setTimeout(function () {
                    getContactsByIcon(unid, tel1, tel2, '', X, Y);
                }, 500);
                $('body').data('timeoutInIcon', timeoutInIcon);
            }).on('mouseleave', '.total-ads-count.contacts-exists .icon', function (e) {
                clearTimeout($('body').data('timeoutInIcon'));

                var timeoutOutContacts = setTimeout(function () {
                    $('.popup-contact-form').hide();
                    $('.popup-contacts-form').hide();
                }, 300);
                $('body').data('timeoutOutContacts', timeoutOutContacts);
            });

            $('body').on('mouseenter', '.popup-contacts-form', function () {
                clearTimeout($('body').data('timeoutOutIcon'));
                clearTimeout($('body').data('timeoutOutContacts'));
            }).on('mouseleave', '.popup-contacts-form', function () {
                var timeoutOutContacts = setTimeout(function () {
                    $('.popup-contact-form').hide();
                    $('.popup-contacts-form').hide();
                }, 300);
                $('body').data('timeoutOutContacts', timeoutOutContacts);
            });

            $(this).on('mouseenter', '.item', function (e) {
                clearTimeout($('body').data('timeoutOutIcon'));
                clearTimeout($('body').data('timeoutOutContact'));
                clearTimeout($('body').data('timeoutOutContacts'));

                var c_id = $(this).find('.id').text();
                var num = $(this).find('.num').text();
                var coord2 = this.getBoundingClientRect();

                $('.popup-contact-form .contact').remove();
                $('.popup-contact-form .note').remove();
                $('.popup-contact-form').prepend(contact_html[num]);

                var X = coord2.left + 150;
                var Y = coord2.top;
                pos_cont_form(X, Y);
                $('.popup-contact-form').show();

            }).on('mouseleave', '.item', function (e) {
                var timeoutOutContact = setTimeout(function () {
                    $('.popup-contact-form').hide();
                }, 300);
                $('body').data('timeoutOutContact', timeoutOutContact);
            });

            $('body').on('mouseenter', '.popup-contact-form', function () {
                clearTimeout($('body').data('timeoutOutContact'));
                clearTimeout($('body').data('timeoutOutContacts'));
            }).on('mouseleave', '.popup-contact-form', function () {
                var timeoutOutContact = setTimeout(function () {
                    $('.popup-contact-form').hide();
                }, 300);
                $('body').data('timeoutOutContact', timeoutOutContact);
                var timeoutOutContacts = setTimeout(function () {
                    $('.popup-contacts-form').hide();
                }, 300);
                $('body').data('timeoutOutContacts', timeoutOutContacts);
            });

            var getFullContactInfo = function (current_kart_id, current_contact) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_getFullContactInfo',
                        cur_contactid: current_contact,
                        cur_kartid: current_kart_id
                    },
                    beforeSend: function (xhr) {
                        $(".contacts").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            $(".overlay-main").show();

                            var c_data = response.contacts_data;
                            c_data.forEach(function (val, i) {
                                $(".popup-contact-edit .c-name").val(val['fio']);
                                $(".popup-contact-edit .c-address").val(val['adres']);
                                $(".popup-contact-edit .c-tel1").val(val['telefon1_format']);
                                $(".popup-contact-edit .c-tel2").val(val['telefon2_format']);
                                $(".popup-contact-edit .c-email").val(val['email']);
                                $(".popup-contact-edit .c-primechanie").val($.trim(val['primechanie']));

                                contact_status = 1;
                                if (val['is_buyer'] == 1) {
                                    contact_status = 0;
                                }
                                if (val['is_seller'] == 1) {
                                    contact_status = 1;
                                }
                                $('.popup-contact-edit .c-status option[value=' + contact_status + ']').prop("selected", true);
                            });
                            $(".popup-contact-edit").show();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };

            var delContactId = function (c_id) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'delContactId',
                        c_id: c_id
                    },
                    beforeSend: function (xhr) {
                    },
                    success: function (response) {
                        $('.popup-contact-form').hide();
                        $('.popup-contacts-form').hide();

                        // перерисовать значки
                        getYellowMessageGroup(JSON.stringify(unids_in_page));
                    },
                    error: function (xhr) {
                        console.log(xhr);
                    },
                    dataType: 'json'
                }).always(function () {
                });
            };

            var setFullContactInfo = function (current_unid, current_contactid) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_setFullContactInfo',
                        cur_unid: current_unid,
                        cur_contactid: current_contactid,
                        cur_name: $.trim($(".popup-contact-edit .c-name").val()),
                        cur_address: $.trim($(".popup-contact-edit .c-address").val()),
                        cur_tel1: $(".popup-contact-edit .c-tel1").val(),
                        cur_tel2: $(".popup-contact-edit .c-tel2").val(),
                        cur_email: $.trim($(".popup-contact-edit .c-email").val()),
                        cur_status_prodavca: $(".popup-contact-edit .c-status option:selected").val(),
                        cur_prim: $.trim($(".popup-contact-edit .c-primechanie").val())
                    },
                    beforeSend: function (xhr) {
                        $(".popup-contact-edit .c-buttons").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            var status_prodavca = $(".popup-contact-edit .c-status option:selected").val();
                            var container = $('.ufield-contacts .contacts-id').filter(function () {
                                return $(this).text() === current_contactid;
                            });

                            container.parent().find(".contacts-fio").text($.trim($(".popup-contact-edit .c-name").val()));
                            container.parent().find(".contacts-tel").text($(".popup-contact-edit .c-tel1").val());
                            container.parent().find(".status-prodavca-icon").removeClass("contact-buyer").removeClass("contact-seller");

                            (status_prodavca == 1) ? container.parent().find(".status-prodavca-icon").addClass("contact-seller") :
                                container.parent().find(".status-prodavca-icon").addClass("contact-buyer");

                            // рисуем contacts-exists в родительском окне
                            //setYellowMessageInParentWindow(unid);

                            $(".overlay-main").hide();
                            $(".popup-contact-edit").hide();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };

            $('body .popup-contact-form-buttons .edit').click(function () {
                var contact_id = $('.popup-contact-form .id').text();
                var unid = $('.popup-contact-form .unid').text();
                var kart_id = $('.popup-contact-form .kartid').text();

                if (contact_id != 0) {
                    $('.popup-contact-form').hide();
                    $('.popup-contacts-form').hide();

                    $(".popup-contact-edit .c-buttons .c-submit").off('click').on('click', function () {
                        if (c_submit) {
                            c_submit = false;
                            $(".c-buttons .c-submit").removeClass("blue-button");

                            if (typeof(contact_id) != "undefined") {
                                // Сохраняем данные контакта карточки
                                setFullContactInfo(unid, contact_id);
                            } else {
                                alert('Ошибка. Попробуйте выйти и снова зайти в программу.')
                            }
                        }
                    });

                    // Получаем все данные контакта для данной карточки
                    getFullContactInfo(kart_id, contact_id);
                }
            });

            var removeFullContacts = function (contacts_ids) {
                $.ajax({
                    url: 'realty/' + Drupal.settings.ajaxPageState.theme + '/realty.pages.inc',
                    type: 'POST',
                    data: {
                        method: 'print_removeFullContacts',
                        contacts_ids: contacts_ids
                    },
                    beforeSend: function (xhr) {
                        $(".header-preview").append(loader);
                    },
                    success: function (response) {
                        if (response.status == 'ok') {
                            // рисуем mk в родительском окне
                            getYellowMessageGroup(JSON.stringify(unids_in_page));
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown); //Better diagnostics
                    },
                    dataType: 'json'
                }).always(function () {
                    loader.remove();
                });
            };

            /*$('body .popup-contact-form-buttons .del').click(function() {
                var contact_id = $.trim($(this).parents("div.popup-contact-form").find('.id').text());
                delContactId(contact_id);
            });*/

            $('body .popup-contact-form-buttons .del').click(function () {
                var kart_id = $('.popup-contact-form .kartid').text();
                if (kart_id != 0) {
                    $('.popup-contact-form').hide();
                    $('.popup-contacts-form').hide();

                    var contact_ids = [],
                        contact_id = $('.popup-contact-form .id').text();
                    contact_fio = $('.popup-contact-form h2').text();

                    contact_ids.push(contact_id);
                    openPopupDialogBox('<h3>Удаление контакта</h3>', 'Удалить "' + contact_fio + '" ? <br /> Все связи с другой недвижимостью будут также удалены.');

                    $(".dialog-ok").off('click').one('click', function () {
                        $(".overlay-main").hide();
                        $(".dialog-box").hide();

                        // Полностью удаляем выделенные контакты
                        removeFullContacts(JSON.stringify(contact_ids));
                    });
                }
            });

            $('body').on('click', '.popup-contact-edit .c-cancel', function () {
                c_submit = false;
                $(".c-buttons .c-submit").removeClass("blue-button");

                $(".overlay-main").hide();
                $(".popup-contact-edit").hide();
            });

            $('.c-name, .c-address, .c-tel1, .c-tel2, .c-email, .c-status, .c-primechanie').on('keyup change', function () {
                c_submit = true;
                $(".c-buttons .c-submit").addClass("blue-button");
            });

// --end page "data"
        }

        // kill save browser pass
        $("#edit-pass").val('');
        $("#edit-name").val('');
        setTimeout(function () {
            $("#edit-pass").val('');
            $("#edit-name").val('');
        }, 200);
    });

    Drupal.behaviors.realty = Drupal.behaviors.realty || {};
    Drupal.behaviors.realty.attach = function (context, settings) {
        if ($(context).prop("tagName") == 'DIV' || $(context).prop("tagName") === undefined) {
            //datepicker
            $('.bef-datepicker').mask("99.99.9999").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: 'c-70:c+30',
                showOn: "button",
                buttonImage: Drupal.settings.basePath + "realty/realty/images/datepicker-icon.png",
                buttonImageOnly: true,
                dateFormat: "dd.mm.yy",
                setDate: currentDate,
                maxDate: currentDate,
                beforeShow: function (input, inst) {
                },
                onSelect: function (date_str, ui) {
                    $(".date-offset-elements option:first").prop("selected", true);
                }
            });

            $.datepicker.regional['ru'] = {
                closeText: 'Закрыть',
                prevText: '&#x3c;Пред',
                nextText: 'След&#x3e;',
                currentText: 'Сегодня',
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                    'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
                dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
                dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false
            };
            $.datepicker.setDefaults($.datepicker.regional['ru']);

            if (cur_page == "data" && logged_in) {
                $(".tel-find").mask("9999999");
                $("#nomer-doma-search").mask('?999');
                $("#edit-cena-min, #edit-cena-max").numericInput({
                    allowFloat: true,
                    allowNegative: true
                });
                $("#obchaja-plochad-min, #obchaja-plochad-max").numericInput({
                    allowFloat: true,
                    allowNegative: true
                });
                $("#plochad-zhilaja-min, #plochad-zhilaja-max").numericInput({
                    allowFloat: true,
                    allowNegative: true
                });
                $("#plochad-kuhni-min, #plochad-kuhni-max").numericInput({
                    allowFloat: true,
                    allowNegative: true
                });
                $("#etazh-min, #etazh-max").mask('?99');
                $("#etazhnost-min, #etazhnost-max").mask('?99');
                $("#god-postrojki-min, #god-postrojki-max").mask('9999');
            }

            $('body').on(EV, '.close', function () {
                $(".overlay-main").hide();
                $(".banner-big").hide();
            });

            $('body').on(EV, '.close-popup', function () {
                $(".microraion-filter, .stations-filter, .popup-lists-add, .popup-lists-edit").hide();
                if ($(this).parent().attr("class") == "popup-lists-add") {
                    $(".overlay-main").hide();
                }
                if ($(this).parent().attr("class") == "popup-lists-edit") {
                    $(".overlay-main").hide();
                }
                if ($(this).parent().attr("class") == "popup-kart-edit") {
                    $(".overlay-main").hide();
                    $(".popup-kart-edit").hide();
                }
                if ($(this).parent().attr("class") == "popup-contact-edit") {
                    $(".overlay-main").hide();
                    $(".popup-contact-edit").hide();
                }
            });

            $('body').on(EV, '.minimized-popup', function () {
                $(".popup-data").hide();
                $(".popup-data-minimized").show();
                minimized_data_popup = true;
            });

            // back to top
            $(".to-top a").click(function () {
                $("html, body").animate({scrollTop: 0}, 1000);

                return false;
            });

            $(".banner-big").hide();

            // if ($(".banner ul").find('li').length > 0) {
            //     slider = $(".banner-big ul").PikaChoose({
            //         autoPlay:false,
            //         showCaption:false,
            //         transition:[0]
            //     });
            // }


            function show_scrollTop() {
                ($(window).scrollTop() > 0) ? $('.to-top').removeClass("hide") : $('.to-top').addClass("hide");
            }

            $(window).scroll(function (e) {
                show_scrollTop();
            });

            // page ajax
            show_scrollTop();
        }
    };
})(jQuery);
